# ttt-bf
Implementation of an extremely basic tic-tac-toe game in the "brainfuck" language.

# Building
Unless you just happen to use the same directory structure as me (and already have the same compiler), you'll want to edit the Makefile to point to the compiler you're using.
The one already referenced in the file is a brainfuck compiler available in the AUR.  
The set of shell commands before the compiler is called take the tictac.bf file, remove all comments, delete all newlines, tabs, and spaces, and finally, split the file up into 80 character lines.
The reason for removing comments is due to the compiler I chose not ignoring <>,.[]-+ characters inside comments (thus defeating the purpose of commenting them out...).
