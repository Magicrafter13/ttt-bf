a.out: block.bf
	~/software/brainfuck/src/brainfuck-0.1/bfc block.bf -v -a 30

block.bf: oneline.bf
	sed -r 's/(.{80})/\1\n/g' oneline.bf > block.bf

oneline.bf: uncommented.bf
	tr -d '\n\t ' < uncommented.bf > oneline.bf

uncommented.bf: tictac.bf
	sed -r '/^\s*!.*/d; s/ ?!.*//' tictac.bf > uncommented.bf

clean:
	rm -f uncommented.bf oneline.bf block.bf a.out
