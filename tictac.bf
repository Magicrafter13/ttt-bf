! MEMORY MAP
!  0 - boolean, is game running
!  1 - Y
!  2 - o
!  3 - u
!  4 - r
!  5 - <space>
!  6 - m
!  7 - o
!  8 - v
!  9 - e
! 10 - ,
! 11 - <space>
! 12 - [
! 13 - 0
! 14 - -
! 15 - 9
! 16 - ]
! 17 - :
! 18 - \n
! 19 - |
! 20 - +
! 21 - player char (x or o, generated each loop)
! 22 - current player (0 or 1)
! 23 - used for looping (make sure always returns to 0)
! 24 - top left is full (boolean)
! 25 - top left owner
! 26 - top middle
! 27 - top middle
! 28 - free copy (like 23)
! 29 - top right
! 30 - top right
! 31 - middle left
! 32 - middle left
! 33 - free copy (like 23)
! 34 - middle middle
! 35 - middle middle
! 36 - middle right
! 37 - middle right
! 38 - free copy (like 23)
! 39 - bottom left
! 40 - bottom left
! 41 - bottom middle
! 42 - bottom middle
! 43 - free copy (like 23)
! 44 - bottom right
! 45 - bottom right
>>>>>>>>>>>>>>>>>>>++++++ ! CL = 6
[>+++++++<-] ! Add 7 to char
>+<< ! Add 1 more, set to 43 [+]
++++++++++ ! CL = 10
[>++++++++++++<-] ! Add 12 to char
>++++< ! Add 4 more, set to 124 [|]
++++++++++<< ! set to [\n]
+++++++ ! CL = 6
[>++++++++<-] ! Add 8 to char
>++<< ! Add 2 more, set to 58 [:]
+++++++++ ! CL = 9
[>++++++++++<-] ! Add 10 to char
>+++<< ! Add 3 more, set to 93 []]
<< ! Doing 2 more characters at the same time
++++++++ ! CL = 8
[>++++++>++++++>++++++<<<-] ! Add 6 to all three
+++ ! CL = 3
[>>->+++<<<-]< ! Subtract 3 from second, and add 9 to third
+++++++++ ! CL = 9
[>++++++++++<-] ! Add 10 to char
>+<<< ! Add 1 more, set to 91 [[]
++++ ! CL = 4
[>+++++++++++>++++++++<<-]< ! Add 8 to char, set to 32 [ ]
!++++ ! CL = 4
![>+++++++++++<-]< ! Add 11 to char, set to 44 [,]
++++++++++ ! CL = 10
[>++++++++++<-] ! Add 10 to char
>+<< ! Add 1 more, set to 101 [e]
+++++++++ ! CL = 9
[>+++++++++++++<-] ! Add 13 to char
>+<<< ! Add 1 more, set to 118 [v]
+++++++++++ ! CL = 11
[>++++++++++>++++++++++<<-] ! Add 10 to both
>->+<<< ! Subtract 1 from first, and add 1 to second, set to 109 [m] and 111 [o]
++++ ! CL = 4
[>++++++++<-]<<< ! Add 8 to char, set to 32 [ ]
+++++++++++++++++++ ! CL = 19
[>++++++>++++++>++++++<<<-] ! Add 6 to all three, set third to 114 [r]
>--->+++<<< ! Subtract 3 from first, set to 111 [o], and add 3 to second, set to 117 [u]
++++++++ ! CL = 8
[>+++++++++++<-] ! Add 11 to char
>+< ! Add one more, and set to 89 [Y]
+ ! game is running
[
	>.>.>.>. ! print Your
	>. ! print [ ]
	>.>.>.>.>. ! print move,
	>. ! print [ ]
	! --- BLOCK 11 ---
	>>>>>>>>>> ! player char
	>>++++++++++ ! set block 23 to 10
	[<<++++++++++++>>-] ! add 12 to player char, set to 120 [x]
	< ! current player
	[>+<-]>[<+<--------->>-] ! copy current player to block 23, then subtract 9 from player char (if current player is 1)
	<<.[-] ! print player char (x or o), then return it to 0
	<<<<<<<<<< ! player char
	. ! print [ ]
	>.>.>.>.>.>.>. ! print [[0-9]:\n]
	! --- BLOCK 18 ---

	!
	! Top Left
	!
	>>>>> ! block 23, loop util
	++++ ! 4
	[<<++++++++>>-] ! Add 8 to char, set to 32 [ ]
	> ! block 24, top left
	[<+>-]<[ ! If top left is owned by a player
		>+ ! Restore value
		<+++++++ ! +7 (8)
		[<<+++++++++++>>-] ! Add 11 to char, set to 120 [x]
		>> ! block 25, top left owner
		[<<+>>-]<<[ ! If top left owner is 1:
			>>+ ! Restore value
			<<<<--------- ! -9 change x to o
			>>-
		]
	]
	! --- BLOCK 23 ---
	<<.[-] ! Print top left and return to 0
	<<. ! Print divider

	!
	! Top Middle
	!
	>>>>++++ ! 4
	[<<++++++++>>-] ! Add 8 to char, set to 32 [ ]
	>>> ! block 26, top middle
	[>>+<<-]>>[ ! If top middle is owned by a player
		<<+ ! Restore value
		>>+++++++ ! +7 (8)
		[<<<<<<<+++++++++++>>>>>>>-] ! Add 11 to char, set to 120 [x]
		< ! block 27, top middle owner
		[>+<-]>[ ! If top middle owner is 1:
			<+ ! Restore value
			<<<<<<--------- ! -9 change x to o
			>>>>>>>-
		]
	]
	! --- BLOCK 28 ---
	<<<<<<<.[-] ! Print top middle and return to 0
	<<. ! Print divider

	!
	! Top Right
	!
	>>>>++++ ! 4
	[<<++++++++>>-] ! Add 8 to char, set to 32 [ ]
	>>>>>> ! block 29, top right
	[<+>-]<[ ! If top right is owned by a player
		>+ ! Restore value
		<+++++++ ! +7 (8)
		[<<<<<<<+++++++++++>>>>>>>-] ! Add 11 to char, set to 120 [x]
		>> ! Block 30, top right owner
		[<<+>>-]<<[ ! If top right owner is 1:
			>>+ ! Restore value
			<<<<<<<<<--------- ! -9 change x to o
			>>>>>>>-
		]
	]
	! --- BLOCK 28 ---
	<<<<<<<.[-] ! Print top right and return to 0
	<<<. ! Print newline

	>>>>>++ ! 2
	[<<<<<<<<<.>>>>>>.>>>-]<<<<<<<<<. ! Print -+-+-
	>>>>. ! Return to block 18 and print newline

	!
	! Middle Left
	!
	>>>>>++++ ! 4
	[<<++++++++>>-] ! Add 8 to char, set to 32 [ ]
	>>>>>>>> ! Block 31, middle left
	[>>+<<-]>>[ ! If middle left is owned by a player
		<<+ ! Restore value
		>>+++++++ ! +7 (8)
		[<<<<<<<<<<<<+++++++++++>>>>>>>>>>>>-] ! Add 11 to char, set to 120 [x]
		< ! Block 32, middle left owner
		[>+<-]>[ ! If middle left owner is 1:
			<+ ! Restore value
			<<<<<<<<<<<--------- ! -9 change x to o
			>>>>>>>>>>>>-
		]
	]
	! --- BLOCK 33 ---
	<<<<<<<<<<<<.[-] ! Print middle left and return to 0
	<<. ! Print divider

	!
	! Middle Middle
	!
	>>>>++++ ! 4
	[<<++++++++>>-] ! Add 8 to char, set to 32 [ ]
	>>>>>>>>>>> ! block 34, middle middle
	[<+>-]<[ ! If middle middle is owned by a player
		>+ ! Restore value
		<+++++++ ! +7 (8)
		[<<<<<<<<<<<<+++++++++++>>>>>>>>>>>>-] ! Add 11 to char, set to 120 [x]
		>> ! Block 35, middle middle owner
		[<<+>>-]<<[ ! If middle middle owner is 1:
			>>+ ! Restore value
			<<<<<<<<<<<<<<--------- ! -9 change x to o
			>>>>>>>>>>>>-
		]
	]
	! --- BLOCK 33 ---
	<<<<<<<<<<<<.[-] ! Print middle middle and return to 0
	<<. ! Print divider

	!
	! Middle Right
	!
	>>>>++++ ! 4
	[<<++++++++>>-] ! Add 8 to char, set to 32 [ ]
	>>>>>>>>>>>>> ! Block 36, middle right
	[>>+<<-]>>[ ! If middle right is owned by a player
		<<+ ! Restore value
		>>+++++++ ! +7 (8)
		[<<<<<<<<<<<<<<<<<+++++++++++>>>>>>>>>>>>>>>>>-] ! Add 11 to char, set to 120 [x]
		< ! Block 37, middle right owner
		[>+<-]>[ ! If middle right owner is 1:
			<+ ! Restore value
			<<<<<<<<<<<<<<<<--------- ! -9 change x to o
			>>>>>>>>>>>>>>>>>-
		]
	]
	! --- BLOCK 38 ---
	<<<<<<<<<<<<<<<<<.[-] ! Print middle right and return to 0
	<<<. ! Print newline

	>>>>>++ ! 2
	[<<<<<<<<<.>>>>>>.>>>-]<<<<<<<<<. ! Print -+-+-
	>>>>. ! Return to block 18 and print newline

	!
	! Bottom Left
	!
	>>>>>++++ ! 4
	[<<++++++++>>-] ! Add 8 to char, set to 32 [ ]
	>>>>>>>>>>>>>>>> ! block 39, bottom left
	[<+>-]<[ ! If bottom left is owned by a player
		>+ ! Restore value
		<+++++++ ! +7 (8)
		[<<<<<<<<<<<<<<<<<+++++++++++>>>>>>>>>>>>>>>>>-] ! Add 11 to char, set to 120 [x]
		>> ! Block 40, bottom left owner
		[<<+>>-]<<[ ! If bottom left owner is 1:
			>>+ ! Restore value
			<<<<<<<<<<<<<<<<<<<--------- ! -9 change x to o
			>>>>>>>>>>>>>>>>>-
		]
	]
	! --- BLOCK 38 ---
	<<<<<<<<<<<<<<<<<.[-] ! Print bottom left and return to 0
	<<. ! Print divider

	!
	! Bottom Middle
	!
	>>>>++++ ! 4
	[<<++++++++>>-] ! Add 8 to char, set to 32 [ ]
	>>>>>>>>>>>>>>>>>> ! Block 41, bottom middle
	[>>+<<-]>>[ ! If bottom middle is owned by a player
		<<+ ! Restore value
		>>+++++++ ! +7 (8)
		[<<<<<<<<<<<<<<<<<<<<<<+++++++++++>>>>>>>>>>>>>>>>>>>>>>-] ! Add 11 to char, set to 120 [x]
		< ! Block 42, bottom middle owner
		[>+<-]>[ ! If bottom middle owner is 1:
			<+ ! Restore value
			<<<<<<<<<<<<<<<<<<<<<--------- ! -9 change x to o
			>>>>>>>>>>>>>>>>>>>>>>-
		]
	]
	! --- BLOCK 43 ---
	<<<<<<<<<<<<<<<<<<<<<<.[-] ! Print bottom middle and return to 0
	<<. ! Print divider

	!
	! Bottom Right
	!
	>>>>++++ ! 4
	[<<++++++++>>-] ! Add 8 to char, set to 32 [ ]
	>>>>>>>>>>>>>>>>>>>>> ! block 44, bottom right
	[<+>-]<[ ! If bottom right is owned by a player
		>+ ! Restore value
		<+++++++ ! +7 (8)
		[<<<<<<<<<<<<<<<<<<<<<<+++++++++++>>>>>>>>>>>>>>>>>>>>>>-] ! Add 11 to char, set to 120 [x]
		>> ! Block 45, bottom right owner
		[<<+>>-]<<[ ! If bottom right owner is 1:
			>>+ ! Restore value
			<<<<<<<<<<<<<<<<<<<<<<<<--------- ! -9 change x to o
			>>>>>>>>>>>>>>>>>>>>>>-
		]
	]
	! --- BLOCK 43 ---
	<<<<<<<<<<<<<<<<<<<<<<.[-] ! Print bottom right and return to 0
	<<<. ! Print newline

	!< ! Return to block 18

	>>>, ! Get user input in block 21
	>>+++++++ ! 7
	[<<------->>-] ! Subtract 7 from input, has effect of turning ascii 1 into zero.
	<<+[ ! Increment, and run block
		>>>>>>>+<<<<<<< ! Set block 28 to 1
		! number was greater than 1; do this
		-[
			! number was greater than 2; do this
			-[
				! number was greater than 3; do this
				-[
					! number was greater than 4; do this
					-[
						! number was greater than 5; do this
						-[
							! number was greater than 6; do this
							-[
								! number was greater than 7; do this
								-[
									! number was greater than 8; do this
									-[
										! number was greater than 9; do this
										-[
											[-]
											>>>>>>>-<<<<<<< ! Reset block 28 - input was not valid
										]
										! otherwise this will run
										>>>>>>>
										[
											>-[ ! If block 29 was 1, this won't be executed
												+ ! Restore value (of zero)
												<<<<<<<[ ! Block 22, current player
													>+ ! Increment 23 to indicate move made by o
													>>>>>>>+ ! Increment 30 to claim ownership by o
													<<<<<<<<- ! Return to 22
												]
												>[ ! If block 23 was incremented, it means it is 'o's turn.
													<+ ! Restore current player 22
													>-
												]
												+ ! Set 23 to 1, indicating updated board
												>>>>>> ! To block 29
											]
											+ ! Increment 29 (if it was previously 1, we just fixed it, if it was 0, we updated it)
											<- ! Return to block 28 and set to zero
										]
										<<<<<<< ! To block 21
										[-] ! Block 21, back to 0
									]
									! otherwise this will run
									>>>>>>>
									[
										<<-[ ! If block 26 was 1, this won't be executed
											+ ! Restore value (of zero)
											<<<<[ ! Block 22, current player
												>+ ! Copy to 23
												>>>>+ ! Increment 27 to claim ownership by o
												<<<<<- ! Return to 22
											]
											>[ ! Restore value in 22
												<+
												>-
											]
											+ ! Set 23 to 1, indicating updated board
											>>> ! To block 26
										]
										+ ! Increment 26 (if it was previously 1, we just fixed... see above)
										>>- ! Return to block 28 and set to zero
									]
									<<<<<<< ! To block 21
									!- ! Block 21, back to 0
								]
								! otherwise this will run
								>>>>>>>
								[
									<<<<-[ ! If block 24 was 1, this won't be executed
										+ ! Restore value (of zero)
										<<[ ! Block 22, current player
											>+ ! Copy to 23
											>>+ ! Increment 25 to claim ownership by o
											<<<- ! Return to 22
										]
										>[ ! Restore value in 22
											<+
											>-
										]
										+ ! Set 23 to 1, indicating updated board
										> ! To block 24
									]
									+ ! Increment 24 (see previous comments)
									>>>>- ! Return to block 28 and set to zero
								]
								<<<<<<< ! To block 21
							]
							! otherwise this will run
							>>>>>>>
							[
								>>>>>>>>-[ ! If block 36 was 1, this won't be executed
									+ ! Restore value (of zero)
									<<<<<<<<<<<<<<[ ! BLock 22, current player
										>+ ! Copy to 23
										>>>>>>>>>>>>>>+ ! Increment 37 to claim ownership by o
										<<<<<<<<<<<<<<<- ! Return to 22
									]
									>[ ! Restore value in 22
										<+
										>-
									]
									+ ! Set 23 to 1, indicating updated board
									>>>>>>>>>>>>> ! To block 36
								]
								+ ! Increment 36 (see above)
								<<<<<<<<- ! Return to block 28 and set to zero
							]
							<<<<<<< ! To block 21
						]
						! otherwise this will run
						>>>>>>>
						[
							>>>>>>-[ ! If block 34 was 1, this won't be executed
								+ ! Restore value (of zero)
								<<<<<<<<<<<<[ ! Block 22, current player
									>+ ! Copy to 23
									>>>>>>>>>>>>+ ! Increment 35 to claim ownership by o
									<<<<<<<<<<<<<- ! Return to 22
								]
								>[ ! Restore value in 22
									<+
									>-
								]
								+ ! Set 23 to 1, indicating updated board
								>>>>>>>>>>> ! To block 34
							]
							+ ! Increment 34 (same deal)
							<<<<<<- ! Return to block 28 and set to zero
						]
						<<<<<<< ! To block 21
					]
					! otherwise this will run
					>>>>>>>
					[
						>>>-[ ! If block 31 was 1, this won't be execute
							+ ! Restore value (of zero)
							<<<<<<<<<[ ! Block 22, current player
								>+ ! Copy to 23
								>>>>>>>>>+ ! Increment 32 to claim ownership by o
								<<<<<<<<<<- ! Return to 22
							]
							>[ ! Restore value in 22
								<+
								>-
							]
							+ ! Set 23 to 1, indicating updated board
							>>>>>>>> ! To block 31
						]
						+ ! Increment 31 (sigh)
						<<<- ! Return to block 28 and set to zero
					]
					<<<<<<< ! To block 21
				]
				! otherwise this will run
				>>>>>>>
				[
					>>>>>>>>>>>>>>>>-[ ! If block 44 was 1, this won't be executed
						+ ! Restore value (of zero)
						<<<<<<<<<<<<<<<<<<<<<<[ ! Block 22, current player
							>+ ! Copy to 23
							>>>>>>>>>>>>>>>>>>>>>>+ ! Increment 45 to claim ownership by o
							<<<<<<<<<<<<<<<<<<<<<<<- ! Return to 22
						]
						>[ ! Restore value in 22
							<+
							>-
						]
						+ ! Set 23 to 1, indicating updated board
						>>>>>>>>>>>>>>>>>>>>> ! To block 44
					]
					+ ! Increment 44 (almost done)
					<<<<<<<<<<<<<<<<- ! Return to block 28 and set to zero
				]
				<<<<<<< ! To block 21
			]
			! otherwise this will run
			>>>>>>>
			[
				>>>>>>>>>>>>>-[ ! If block 41 was 1, this won't be executed
					+ ! Restore value (of zero)
					<<<<<<<<<<<<<<<<<<<[ ! Block 22, current player
						>+ ! Copy to 23
						>>>>>>>>>>>>>>>>>>>+ ! Increment 42 to claim ownership by o
						<<<<<<<<<<<<<<<<<<<<- ! Return to 22
					]
					>[ ! Restore value in 22
						<+
						>-
					]
					+ ! Set 23 to 1, indicating updated board
					>>>>>>>>>>>>>>>>>> ! To block 41
				]
				+ ! Increment 41 (AAAAAAAAA)
				<<<<<<<<<<<<<- ! Return to block 28 and set to zero
			]
			<<<<<<< ! To block 21
		]
		! otherwise this will run
		>>>>>>>
		[
			>>>>>>>>>>>-[ ! If block 39 was 1, this won't be executed
				+ ! Restore value (of zero)
				<<<<<<<<<<<<<<<<<[ ! Block 22, current player
					>+ ! Copy to 23
					>>>>>>>>>>>>>>>>>+ ! Increment 40 to claim ownership by o
					<<<<<<<<<<<<<<<<<<- ! Return to 22
				]
				>[ ! Restore value in 22
					<+
					>-
				]
				+ ! Set 23 to 1, indicating updated board
				>>>>>>>>>>>>>>>> ! To block 39
			]
			+ ! Increment 39 (FINISHED!)
			<<<<<<<<<<<- ! Return to block 28 and set to zero
		]
		<<<<<<< ! To block 21

		>>>>>>>[-]<<<<<<< ! Set block 28 to 0

		>>[ ! If board has changed, 23 will be 1
			- ! Set to zero so we can use this block

			! check for win
			>[ ! if block24
				>[<<+>>-]<<[<<+>>>>+<<-]> ! block21 += block25
				! HORIZONTAL - TOP ROW
				>>[ ! if block26
					>[<<<<+>>>>-]<<<<[<<+>>>>>>+<<<<-]>>> ! block21 += block27
					!<<<<[>+<-]>[<+<+>>-]>>> ! block21 += block22
					>>>[ ! if block29
						>[<<<<<<<+>>>>>>>-]<<<<<<<[<<+>>>>>>>>>+<<<<<<<-] ! block21 += block30
						!<<<<<<<[>+<-]>[<+<+>>-] ! block21 += block22
						<<[>>+>>>>>>>>>>+<<<<<<<<<<<<-]>>[<<+>>-] ! block33 = block21
						<[->+<<--->]>[<+>-] ! block21 -= (block23 = block22) * 3
						<<[>>-<<[-]] ! if (block21 != 0) block23 = (block21 = 0)-1
						>>+[ ! if (block23 += 1)
							>>>>>+ ! block28++
							<<<<<- ! block23-- (should now be 0)
						]

						!<[->+<<+++>]>[<+>-] ! block21 += (block23 = block22) * 3
						>>>>>>>>>>[<<<<<<<<<<<<+>>>>>>>>>>>>-] ! move block33 to block21
						<<<[<<<<<<<+>>>>>>>-]<<<<<<<[<<->>>>>>>>>+<<<<<<<-] ! block21 -= block30
						+ ! block23 = 1
						>>>>>>- ! block29 = 0
					] ! ends at block 29
					! If top right cell was owned by a player, we must restore it
					<<<<<<[>>>>>>+<<<<<<-]

					>>>>[<<<<+>>>>-]<<<<[<<->>>>>>+<<<<-] ! block21 -= block27
					+ ! block23 = 1
					>>>- ! block26 = 0
				] ! ends at block 26
				! if top middle cell was owned by a player, we must restore it
				<<<[>>>+<<<-]
				! DIAGONAL - TOP LEFT TO BOTTOM RIGHT
				>>>>>>>>>>>[ ! if block34
					>[<<<<<<<<<<<<+>>>>>>>>>>>>-]<<<<<<<<<<<<[<<+>>>>>>>>>>>>>>+<<<<<<<<<<<<-]>>>>>>>>>>> ! block21 += block35
					>>>>>>>>>>[ ! if block44
						>[<<<<<<<<<<<<<<<<<<<<<<+>>>>>>>>>>>>>>>>>>>>>>-]<<<<<<<<<<<<<<<<<<<<<<[<<+>>>>>>>>>>>>>>>>>>>>>>>>+<<<<<<<<<<<<<<<<<<<<<<-] ! block21 += block45
						<<[>>+>>>>>>>>>>+<<<<<<<<<<<<-]>>[<<+>>-] ! block33 = block21
						<[->+<<--->]>[<+>-] ! block21 -= (block23 = block22) * 3
						<<[>>-<<[-]] ! if (block21 != 0) block23 = (block21 = 0)-1
						>>+[ ! if (block23 += 1)
							>>>>>+ ! block28++
							<<<<<- ! block23-- (should now be 0)
						]

						!<[->+<<+++>]>[<+>-] ! block21 += (block23 = block22) * 3
						>>>>>>>>>>[<<<<<<<<<<<<+>>>>>>>>>>>>-] ! move block33 to block21
						>>>>>>>>>>>>[<<<<<<<<<<<<<<<<<<<<<<+>>>>>>>>>>>>>>>>>>>>>>-]<<<<<<<<<<<<<<<<<<<<<<[<<->>>>>>>>>>>>>>>>>>>>>>>>+<<<<<<<<<<<<<<<<<<<<<<-] ! block21 -= block45
						+ ! block23 = 1
						>>>>>>>>>>>>>>>>>>>>>- ! block44 = 0
					] ! ends at block 44
					! If bottom right cell was owned by a player, we must restore it
					<<<<<<<<<<<<<<<<<<<<<[>>>>>>>>>>>>>>>>>>>>>+<<<<<<<<<<<<<<<<<<<<<-]

					>>>>>>>>>>>>[<<<<<<<<<<<<+>>>>>>>>>>>>-]<<<<<<<<<<<<[<<->>>>>>>>>>>>>>+<<<<<<<<<<<<-] ! block21 -= block35
					+ ! block23 = 1
					>>>>>>>>>>>- ! block34 = 0
				] ! ends at block 34
				! If middle middle cell was owned by a player, we must restore it
				<<<<<<<<<<<[>>>>>>>>>>>+<<<<<<<<<<<-]
				! VERTICAL - LEFT COLUMN
				>>>>>>>>[ ! if block31
					>[<<<<<<<<<+>>>>>>>>>-]<<<<<<<<<[<<+>>>>>>>>>>>+<<<<<<<<<-]>>>>>>>>> ! block21 += block32
					>>>>>>>[ ! if block39
						>[<<<<<<<<<<<<<<<<<+>>>>>>>>>>>>>>>>>-]<<<<<<<<<<<<<<<<<[<<+>>>>>>>>>>>>>>>>>>>+<<<<<<<<<<<<<<<<<-] ! block21 += block40
						<<[>>+>>>>>>>>>>+<<<<<<<<<<<<-]>>[<<+>>-] ! block33 = block21
						<[->+<<--->]>[<+>-] ! block21 -= (block23 = block22) * 3
						<<[>>-<<[-]] ! if (block21 != 0) block23 = (block21 = 0)-1
						>>+[ ! if (block23 += 1)
							>>>>>+ ! block28++
							<<<<<- ! block23-- (should now be 0)
						]

						>>>>>>>>>>[<<<<<<<<<<<<+>>>>>>>>>>>>-] ! move block33 to block21
						>>>>>>>[<<<<<<<<<<<<<<<<<+>>>>>>>>>>>>>>>>>-]<<<<<<<<<<<<<<<<<[<<->>>>>>>>>>>>>>>>>>>+<<<<<<<<<<<<<<<<<-] ! block21 -= block40
						+ ! block23 = 1
						>>>>>>>>>>>>>>>>- ! block39 = 0
					] ! ends at block 39
					! If bottom left cell was owned by a player, we must restore it
					<<<<<<<<<<<<<<<<[>>>>>>>>>>>>>>>>+<<<<<<<<<<<<<<<<-]

					>>>>>>>>>[<<<<<<<<<+>>>>>>>>>-]<<<<<<<<<[<<->>>>>>>>>>>+<<<<<<<<<-] ! block21 -= block32
					+ ! block23 = 1
					>>>>>>>>- ! block31 = 0
				] ! ends at block 31
				! If middle left cell was owned by a player, we must restore it
				<<<<<<<<[>>>>>>>>+<<<<<<<<-]

				>>[<<+>>-]<<[<<->>>>+<<-] ! block21 -= block25
				+ ! block23 = 1
				>- ! block24 = 0
			] ! ends at block 24
			! if top middle cell was owned by a player, we must restore it
			<[>+<-]
			>>>>>[ ! if (block28)
				<<<<<++++++++++ ! set block 23 to 10
				[<<++++++++++++>>-] ! add 12 to player char, set to 120 [x]
				< ! current player
				[>+<-]>[<+<--------->>-] ! copy current player to block 23, then subtract 9 from player char (if current player is 1)
				<<. ! print player char (x or o), then return it to 0
				>> ! To block 23
				+++++++[<<<<<<++++++++>>>>>>-]<<<<<<+>>>>>> ! add 57 to : to become s
				++++[<<<<<<<++++>>>>>>>-]<<<<<<<+>>>>>>> ! add 17 to ] to become n
				++++++[<<<<<<<<++++++++>>>>>>>>-] ! add 48 to 9 to become i
				++++++++[<<<<<<<<<+++++++++>>>>>>>>>-]<<<<<<<<<++>>>>>>>>> ! add 74 to - to become w
				<<<<<<<<<<<<<<<<<<<<<<<- ! end game, block 0 = 0
				>>>>>>>>>>>. ! print space (block 11)
				>>>.>.>.>.>. ! print wins\n
				>>>>>>>>>>[-] ! block 28 = 0
			]
			<<<<< ! To block 23

			! Change current player
			<[>+<-] ! Copy block 22 to 23
			+ ! Set block 22 to 1
			>[<->-] ! If 22 was 1 before, now it should be 0
		]
		<< ! Return to block 21
	]
	+[,----------] ! Wait for \n
	<<< ! Back to block 18

	! end
	<<<<<<<<<<<<<<<<<< ! return to block 0
	![-] ! end game
]
